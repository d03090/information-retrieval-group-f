/**
* This phase creates a new annotation date2 for all Date annotations. But also
* some Date-annotations are joined into one new date2-annotation. This is needed
* because sometimes strings like August 1 st, 1998 are annotated as two seperate
* Date-annotations but will be just one date2 annotation after this phase.
*
* Authors: Leitner Thomas, Stangl Stefan
*/
Phase: date2
Input: Token Date Delimiter
Options: control = appelt

/**
* If a Date is followed by st, nd, rd, th or any delimiters and this in turn
* is followed by a Year, then the whole string is annotated as date2.
* date2 is later used to find workshop-dates and paper-submission-dates.
*/
Rule: date2a
(
	((({Date}):dateorig)
	((  {Delimiter} | 
		{Token.kind == number} | 
		{Token.string ==~ "([Ss][Tt])"} | 
		{Token.string ==~ "([Nn][Dd])"} | 
		{Token.string ==~ "([Rr][Dd])"} | 
		{Token.string ==~ "([Tt][Hh])"} )*)
	({Date.rule2 == YearOnlyFinal})):d
):tmp
-->
:d.date2 = {rule2 = :dateorig.Date.rule2}

/**
* The default rule, if there is nothing like st, nd, rd of th between 
* two dates
*/
Rule: date2b
(
	({Date}):d
):tmp
-->
:d.date2 = {rule2 = :d.Date.rule2}