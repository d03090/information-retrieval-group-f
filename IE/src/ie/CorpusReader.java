package ie;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import exceptions.RootFolderNotFoundException;
import gate.Document;
import gate.Factory;
import gate.creole.ResourceInstantiationException;

/**
 * This class implements the reader that can return URLs from the document
 * collection. The CorpusReader maintains a list of all documents to read from
 * and reads sequentially through all of them.
 * 
 * @author Leitner Thomas
 * @author Stangl Stefan
 * 
 */
public class CorpusReader {
   /**
    * The path to the root folder of the document collection.
    */
   private String    _rootFolder = null;
   /**
    * The list of all file urls for all documents.
    */
   private List<URL> _fileURLs   = null;

   /**
    * The amount of files in the rootFolder
    */
   public int        FileCount   = 0;

   /**
    * The constructor initializes the list of all documents by calling the
    * recursive listFilesForFolder() method by recursively listing all files
    * starting from the rootFolder.
    * 
    * @param rootFolder
    *           the root folder of the document collection
    * @throws IOException
    * @throws RootFolderNotFoundException
    */
   public CorpusReader(String rootFolder) throws IOException,
         RootFolderNotFoundException {
      _rootFolder = rootFolder;
      _fileURLs = new ArrayList<URL>();
      FileCount = 0;

      File folder = new File(_rootFolder);
      if (folder == null || !folder.exists()) {
         throw new RootFolderNotFoundException("Folder does not exist.");
      }

      listFilesForFolder(new File(_rootFolder));

      FileCount = _fileURLs.size();
   }

   /**
    * Recursively traverses all Directories under folder and adds all found
    * files to the _fileURLs list
    * 
    * @param folder
    *           the root folder for traversing
    * @throws IOException
    */
   private void listFilesForFolder(File folder) throws IOException {
      for (File fileEntry : folder.listFiles()) {
         if (fileEntry.isDirectory()) {
            listFilesForFolder(fileEntry);
         } else {
            _fileURLs.add(fileEntry.toURI().toURL());
         }
      }
   }

   /**
    * Iterates through the document collection and returns the next N documents
    * and deletes them from the list.
    * 
    * @param N
    * @return a list of N document urls (if N is greater than the size of the
    *         document list, the remaining document urls will be
    *         returned)
    */
   public ArrayList<Document> GetNextNDocuments(int N) {
      ArrayList<Document> ret = new ArrayList<Document>();

      Document doc = null;
      while (N > 0 && _fileURLs.size() > 0) {
         try {
            //create a new GATE document and remove the url from the fileURLs list
            doc = Factory.newDocument(_fileURLs.get(0));
            _fileURLs.remove(0);
         } catch (ResourceInstantiationException e) {
            e.printStackTrace();
         }

         ret.add(doc);

         N--;
      }

      return ret;
   }
}
