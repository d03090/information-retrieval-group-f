package ie;


import java.io.IOException;

import exceptions.RootFolderNotFoundException;

import gate.util.GateException;
import gnu.getopt.Getopt;
import gnu.getopt.LongOpt;

/**
 * The Main class
 * 
 * @author Leitner Thomas
 * @author Stangl Stefan
 * 
 */
public class Main {

	/**
	 * This is the main method of the application. The command line arguments
	 * are parsed and then a new Application-object is instantiated and the
	 * startApplication() method is called
	 * 
	 * @param args
	 *            the command line arguments, they are parsed using the getopt
	 *            library
	 */
	public static void main(String[] args) {
		// initialising with default values
		int N = 5;
		String corpusDir = "train";
		String outFile = "out.txt";

		// the array for the long options
		LongOpt[] longOpts = new LongOpt[] {
				new LongOpt("doc-count", LongOpt.REQUIRED_ARGUMENT, null, 'n'),
				new LongOpt("corpus-dir", LongOpt.REQUIRED_ARGUMENT, null, 'c'),
				new LongOpt("out-file", LongOpt.REQUIRED_ARGUMENT, null, 'o'),
				new LongOpt("help", LongOpt.NO_ARGUMENT, null, 'h') };
		Getopt g = new Getopt("task4.jar", args, "n:c:o:", longOpts);

		int c;
		while ((c = g.getopt()) != -1) {
			switch (c) {
			case 'n':
				try {
					N = Integer.parseInt(g.getOptarg());
				} catch (Exception ex) {
					printUsage();
					System.exit(1);
				}
				break;
			case 'c':
				corpusDir = g.getOptarg();
				break;
			case 'o':
				outFile = g.getOptarg();
				break;
			case 'h':
				printUsage();
				System.out
						.println("Default values are: N=5, corpus-dir=\"train\", out-file=\"out\"");
				System.exit(0);
				break;
			case '?':
				printUsage();
				System.exit(1);
				break;
			default:
				System.out.print("getopt() returned " + c + "\n");
				System.exit(1);
			}
		}

		Application application = new Application();

		try {
			application.startApplication(N, corpusDir, outFile);
		} catch (GateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RootFolderNotFoundException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Prints the Usage message.
	 */
	private static void printUsage() {
		System.out
				.println("Usage: java -jar task4.jar [--doc-count=<N>] [--corpus-dir=<corpusDirectory>] [--out-file=<output-file>] [--help]");
	}

}