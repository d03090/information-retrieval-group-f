package ie;

import exceptions.RootFolderNotFoundException;
import gate.Annotation;
import gate.AnnotationSet;
import gate.Corpus;
import gate.Document;
import gate.Factory;
import gate.Gate;
import gate.LanguageAnalyser;
import gate.creole.SerialAnalyserController;
import gate.gui.MainFrame;
import gate.util.GateException;
import gate.util.InvalidOffsetException;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import org.apache.log4j.BasicConfigurator;

/**
 * In this class GATE will be loaded and initialized with our tailored ANNIE
 * application. We added our own transducer at the end of the standard ANNIE
 * system. We also write the outFile in this class.
 * 
 * @author Leitner Thomas
 * @author Stangl Stefan
 * 
 */
public class Application {
   /**
    * This method reads the documents from the corpusDir and passes N (or less)
    * documents at a time to GATE. Our adapted ANNIE system will be loaded,
    * configured and started. The results will be saved to the outFile.
    * 
    * @param N
    *           number of Documents that will be loaded into GATE at the same
    *           time. The smaller the value of N is, the less memory will be
    *           used from GATE.
    * @param corpusDir
    *           the directory that contains the document collection
    * @param outFile
    *           file where the results will be saved
    * @throws GateException
    *            when GATE throws an error
    * @throws IOException
    * @throws RootFolderNotFoundException
    *            when the rootFolder is unaccessible
    * @throws InterruptedException
    */
   public void startApplication(int N, String corpusDir, String outFile)
         throws GateException, IOException, RootFolderNotFoundException,
         InterruptedException {
      BasicConfigurator.configure();
      // System.setProperty("gate.home",
      // "C:\\Program Files\\GATE_Developer_7.1");
      
      // prepare the library
      Gate.init();
      // show the main window...
      MainFrame.getInstance().setVisible(true);

      Corpus corpusCFP;

      // loading annie plugin
      System.out.println("loading plugin (annie) ...");
      File pluginsDir = Gate.getPluginsHome();
      File apluginDir = new File(pluginsDir, "ANNIE");
      Gate.getCreoleRegister().registerDirectories(apluginDir.toURI().toURL());

      //loading the AnnotationDeletePR
      LanguageAnalyser prDeleter = (LanguageAnalyser) Factory
            .createResource("gate.creole.annotdelete.AnnotationDeletePR");

      //loading the Tokeniser
      LanguageAnalyser prTokeniser = (LanguageAnalyser) Factory
            .createResource("gate.creole.tokeniser.DefaultTokeniser");
      prTokeniser.setParameterValue("annotationSetName", "IE_ANNIE");

      //loading the Gazetteer
      LanguageAnalyser prGazetteer = (LanguageAnalyser) Factory
            .createResource("gate.creole.gazetteer.DefaultGazetteer");
      prGazetteer.setParameterValue("annotationSetName", "IE_ANNIE");

      //loading the SeneteceSplitter
      LanguageAnalyser prSentenceSplitter = (LanguageAnalyser) Factory
            .createResource("gate.creole.splitter.SentenceSplitter");
      prSentenceSplitter.setParameterValue("inputASName", "IE_ANNIE");
      prSentenceSplitter.setParameterValue("outputASName", "IE_ANNIE");

      //loading the POSTagger
      LanguageAnalyser prPOSTagger = (LanguageAnalyser) Factory
            .createResource("gate.creole.POSTagger");
      prPOSTagger.setParameterValue("inputASName", "IE_ANNIE");
      prPOSTagger.setParameterValue("outputASName", "IE_ANNIE");

      //loading the standard annie transducer
      LanguageAnalyser prTransducer = (LanguageAnalyser) Factory
            .createResource("gate.creole.ANNIETransducer");
      prTransducer.setParameterValue("inputASName", "IE_ANNIE");
      prTransducer.setParameterValue("outputASName", "IE_ANNIE");

      //loading the OrthoMatcher
      LanguageAnalyser prOrthoMatcher = (LanguageAnalyser) Factory
            .createResource("gate.creole.orthomatcher.OrthoMatcher");
      prOrthoMatcher.setParameterValue("annotationSetName", "IE_ANNIE");

      //loading an standard annie transducer with our defined grammar.
      LanguageAnalyser prCPFTransducer = (LanguageAnalyser) Factory
            .createResource("gate.creole.ANNIETransducer");
      prCPFTransducer.setParameterValue("inputASName", "IE_ANNIE");
      prCPFTransducer.setParameterValue("outputASName", "IE_ANNIE");
      prCPFTransducer.setParameterValue("grammarURL", new File(
            "grammar/main.jape").toURI().toURL());
      prCPFTransducer.reInit();

      //creating a controller that will execute the individual resources step by step
      SerialAnalyserController controller = (SerialAnalyserController) Factory
            .createResource("gate.creole.SerialAnalyserController");

      //adding the resources to the controller
      System.out.println("adding processing resources ...");
      controller.add(prDeleter);
      controller.add(prTokeniser);
      controller.add(prGazetteer);
      controller.add(prSentenceSplitter);
      controller.add(prPOSTagger);
      controller.add(prTransducer);
      controller.add(prOrthoMatcher);
      controller.add(prCPFTransducer);

      //initialize the corpusReader
      CorpusReader corpusReader = new CorpusReader(corpusDir);

      //opening the outFile
      PrintWriter pw = new PrintWriter(outFile);

      ArrayList<Document> docs;
      int i = 0;
      //get N documents from the CorpusReader until no documents are left
      while ((docs = corpusReader.GetNextNDocuments(N)).size() > 0) {
         i += docs.size();
         
         //setting a new corpus and adding N documents to it
         corpusCFP = Factory.newCorpus("CFP");
         corpusCFP.addAll(docs);
         controller.setCorpus(corpusCFP);

         //running our GATE program
         controller.execute();
         
         //progress output
         System.out.println("Pipeline executed for "
               + i
               + " of "
               + corpusReader.FileCount
               + " documents ("
               + String.format("%.2f", ((float) i
                     / (float) corpusReader.FileCount * 100.0)) + "%) ...");

         //printing the results for one document and dispose the object
         for (Document d : docs) {
            pw.println(d.getName() + ":");
            pw.println("---");
            AnnotationSet set = d.getAnnotations("IE_ANNIE");
            writeAnnotation(pw, d, set, "workshopname");
            writeAnnotation(pw, d, set, "workshopacronym");
            writeAnnotation(pw, d, set, "workshopdate");
            writeAnnotation(pw, d, set, "workshoplocation");
            writeAnnotation(pw, d, set, "conferencename");
            writeAnnotation(pw, d, set, "conferenceacronym");
            writeAnnotation(pw, d, set, "workshoppapersubmissiondate");

            pw.println("##################################");
            pw.println("");

            Factory.deleteResource(d);
         }

         //dispose the corpus
         Factory.deleteResource(corpusCFP);
      }

      pw.close();

      System.out.println("Finished executing pipeline ...");
   }

   /**
    * Writes one line in the outFile containing the information for the
    * specified annotationname in the form:
    * [annotationname]: [content of the annotation]
    * 
    * The first entry of the annotation set will be our result. 
    * 
    * @param pw opened PrintWriter for writing into the outFile 
    * @param d current Document that is processed in GATE
    * @param set Set of Annotations of the current document
    * @param annotationname name of the annotation whose value should be printed.
    * @throws InvalidOffsetException
    */
   private void writeAnnotation(PrintWriter pw, Document d, AnnotationSet set,
         String annotationname) throws InvalidOffsetException {
      pw.print(annotationname + ": ");
      //if annotation exists
      if (set.get(annotationname).iterator().hasNext()) {
         //get the first entry and print it
         Annotation annotation = set.get(annotationname).iterator().next();
         pw.println(d.getContent().getContent(
               annotation.getStartNode().getOffset(),
               annotation.getEndNode().getOffset()));
      } else {
         pw.println("");
      }
   }

}
