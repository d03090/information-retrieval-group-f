package indexer;

import java.util.HashMap;

/**
 * This class implements the data structure of one line in the final arff-file.
 * It consists of a documentName, the documentClass and a HashMap of
 * Term-Weights for all terms that occur in the document. In the arff file it is
 * of the following form:
 * documentName,className,weightTerm1,weightTerm2,...,weightTermM
 * 
 * @author Leitner Thomas
 * @author Stangl Stefan
 * 
 */
public class ArffData {
   /**
    * The name of the document
    */
   private String                  documentName;
   /**
    * The classname of the document
    */
   private String                  className;
   /**
    * A  HashMap of the weights for all terms that occur in the document
    */
   private HashMap<String, Double> termWeights;

   public String getDocumentName() {
      return documentName;
   }

   public void setDocumentName(String documentName) {
      this.documentName = documentName;
   }

   public String getClassName() {
      return className;
   }

   public void setClassName(String className) {
      this.className = className;
   }

   public HashMap<String, Double> getTermWeights() {
      return termWeights;
   }

   public void setTermWeights(HashMap<String, Double> termWeights) {
      this.termWeights = termWeights;
   }
}
