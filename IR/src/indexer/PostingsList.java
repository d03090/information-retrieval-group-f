package indexer;

import java.util.*;

/**
 * This class implements the PostingsList for a certain term. A posting consists
 * of a documentName and the Term-Frequency. The class extends ArrayList and
 * provides two additional methods for inserting a Posting and merging the
 * current List with another List. The postings are ordered ascending by
 * DocumentName
 * 
 * @author Leitner Thomas
 * @author Stangl Stefan
 * 
 */
public class PostingsList extends ArrayList<Posting> {
   /**
    * serial version id for serializing
    */
   private static final long serialVersionUID = 1L;

   public PostingsList() {

   }

   /**
    * This method updates the posting list by adding an occurence in the
    * Document identified by documentName. If the document already is part of
    * the list, the term frequency in the corresponding posting is incremented.
    * If it isn't already a part of the list, it is added to the list with term
    * frequency set to 1. If a new Posting is inserted, it is inserted ordered
    * ascending by documentName.
    * 
    * @param documentName
    *           the name of the Document to be inserted
    */
   public void addPosting(String documentName) {
      Posting p;
      boolean found = false;
      for (int i = 0; i < this.size() && !found; i++) {
         p = this.get(i);
         if (p.getDocumentName().equals(documentName)) {
            p.IncrementTermFrequency();
            found = true;
         } else if (p.getDocumentName().compareTo(documentName) > 0) {
            this.add(i, new Posting(documentName, 1L));
            found = true;
         }
      }
      if (!found)
         this.add(new Posting(documentName, 1L));
   }

   /**
    * This method merges the PostingsList pl into the current PostingsList. The
    * merging retains the ordering of the Postings.
    * 
    * @param pl
    *           the other PostingsList to be merged into the current
    *           PostingsList
    */
   public void merge(PostingsList pl) {
      Posting p1;
      Posting p2;
      int i = 0;
      int j = 0;

      while (i < this.size()) {
         p1 = this.get(i);

         while (j < pl.size()
               && ((!(pl.get(j).compareTo(p1) > 0) || (i == this.size() - 1)))) {
            p2 = pl.get(j);
            if (p2.equals(p1)) {
               p1.increaseTermFrequency(p2.getTermFrequency());
            } else if (p2.compareTo(p1) < 0) {
               this.add(i, p2);
               i++;
            } else {
               this.add(i + 1, p2);
               i++;
            }
            j++;
         }
         i++;

      }

   }
}
