package indexer;

import java.util.*;

/**
 * This class implements the data structure that is used to store one block of
 * terms with their corresponding postings list in RAM. The dictionary later is
 * written to disk as a temporary block file. The size of the dictionary in
 * memory should not exceed the constant AvailableMemory defined in Config.java.
 * This is only implemented as a simulation, because it is not quite easy to
 * determine the actual size of the dictionary in memory.
 * 
 * @author Leitner Thomas
 * @author Stangl Stefan
 * 
 */
public class Dictionary {
   /**
    * The data structure for the Dictionary. A TreeMap is used, because it
    * basically is a sorted HashMap. The keys (terms) must have an order, so
    * that they can be merged later on. All important operations on a TreeMap
    * like containsKey, get, put can be performed in log(n)
    */
   private TreeMap<String, PostingsList> _data;

   /**
    * The default Constructor. The internal TreeMap is initialized. The keys
    * represent the terms and the values are the corresponding PostingsLists.
    */
   public Dictionary() {
      _data = new TreeMap<String, PostingsList>();
   }

   /**
    * This method returns the internal TreeMap
    * 
    * @return the internal TreeMap
    */
   public TreeMap<String, PostingsList> GetDictionary() {
      return _data;
   }
}
