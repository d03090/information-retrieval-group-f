package indexer;

import java.io.IOException;
import java.util.HashMap;

import exceptions.RootFolderNotFoundException;

/**
 * This class implements the stream of tokens, which delivers one token at a
 * time through the getNextToken() method. The tokens are normalized which is
 * mainly be done by transforming them to only lower case letters and stemming.
 * 
 * @author Leitner Thomas
 * @author Stangl Stefan
 * 
 */
public class TokenStream {
   /**
    * The underlying reader. It reads from the document collection and delivers
    * one character at a time. The reader takes care of the documents by itself,
    * so the TokenStream doesn't have to worry about different documents.
    */
   private CorpusReader             _reader              = null;
   /**
    * Flat to indicate if a new document has been opened since the last time a
    * token has been read completely. If this flag is set to true, this means
    * that the currently read token is the first token of a new document.
    */
   private boolean                  newDoc               = false;
   /**
    * The average length of the documents in the collection. This value is
    * calculated on the fly while reading all tokens from the collection. The
    * calculation is done in the getNextToken() method.
    */
   private double                   avgDocLength         = 0d;
   /**
    * The token count of the currently read document.
    */
   private int                      currentDocTokenCount = 0;
   /**
    * This HashMap stores the token count for each document of the collection.
    * This information is needed for calculating the weights of the terms in the
    * index.
    */
   private HashMap<String, Integer> docLengths           = null;

   /**
    * In the constructor the underlying Corpusreader and the docLengths-HashMap
    * are initialized.
    * 
    * @param rootFolder
    *           the root folder of the document collection
    * @throws RootFolderNotFoundException
    *            is thrown if rootFolder doesn't exist
    * @throws IOException
    *            is thrown if any other IOError is encountered by the reader
    */
   public TokenStream(String rootFolder) throws RootFolderNotFoundException,
         IOException {
      _reader = new CorpusReader(rootFolder, this);
      docLengths = new HashMap<String, Integer>();
   }

   /**
    * This method reads a new token from the underlying CorpusReader. The method
    * reads one character at a time from _reader. As soon as a token is found it
    * is returned. Token are split by all non-alphanumerical characters.
    * 
    * @return a new token
    * @throws IOException
    */
   public Token getNextToken() throws IOException {
      Token ret = new Token();
      StringBuilder token = new StringBuilder();

      boolean stop = false;
      int c = -1;
      c = _reader.read(); // -1 if end of collection is reached
      while (c != -1 && !stop) {

         // check if c is delimiter or not...
         if (!isDelimiter(c)) {
            token.append((char) c);
         } else if (token.length() > 0) {
            stop = true; // if c is delimiter and token-length >= 1 we have
                         // completed another token!
         }

         // if the last read token was no delimiter OR if we haven't read
         // anything useful by now, keep reading...
         if (!isDelimiter(c) || token.length() == 0) {
            c = _reader.read();
         }
      }

      // if end of collection is reached and we haven't read anything useful,
      // just return null, we are finished here. But before that check if newDoc
      // is set, to update the average doc-length
      if (c == -1 && token.length() == 0) {
         if (newDoc) {
            newDoc = false;
            avgDocLength += (double) currentDocTokenCount
                  / (double) _reader.GetFileCount();

            if (docLengths.containsKey(_reader.getPreviousFile())) {
               docLengths.put(_reader.getPreviousFile(),
                     docLengths.get(_reader.getPreviousFile())
                           + currentDocTokenCount);
            } else {
               docLengths.put(_reader.getPreviousFile(), currentDocTokenCount);
            }
            currentDocTokenCount = 0;
         }

         return null;
      }

      String cleanedToken = StemmingHelper.Instance().getStemmedTerm(
            getCleanedToken(token.toString()));

      // if after the stemming suddenly the normalized token has length 0,
      // call the method again recursively, otherwise return the just found
      // token
      if (cleanedToken.length() == 0) {
         return getNextToken();
      } else {
         ret.setValue(cleanedToken);
         ret.setDocumentName(_reader.getDocName());
         ret.setClassName(_reader.getClassName());

         // if newDoc is true, this means that the token ret is the first token
         // of a new document. So the average document-length has to be adjusted
         // and the currentTokenCount must be set to 0
         if (newDoc) {
            newDoc = false;
            avgDocLength += (double) currentDocTokenCount
                  / (double) _reader.GetFileCount();

            if (docLengths.containsKey(_reader.getPreviousFile())) {
               docLengths.put(_reader.getPreviousFile(),
                     docLengths.get(_reader.getPreviousFile())
                           + currentDocTokenCount);
            } else {
               docLengths.put(_reader.getPreviousFile(), currentDocTokenCount);
            }
            currentDocTokenCount = 0;
         }

         currentDocTokenCount++;
         return ret;
      }
   }

   /**
    * This method just transforms a given string to only lower case letters. In
    * this form all terms are stored and processed later on.
    * 
    * @param token
    *           the input token value
    * @return the lower case form of the token
    */
   private String getCleanedToken(String token) {
      return token.toLowerCase();
   }

   /**
    * Returns if the given character c is an alphanumerical character.
    * 
    * @param c
    *           the input character
    * @return true if c is non-alphanumerical, false otherwise
    */
   private boolean isDelimiter(int c) {
      String s = ((char) c) + "";

      return s.matches("[^a-zA-Z0-9]");
   }

   /**
    * Returns the total number of documents by calling the corresponding method
    * in the underlying CorpusReader. Originally, only the CorpusReader really
    * knows how many documents there are in the collection.
    * 
    * @return total number of documents in collection
    */
   public long GetFileCount() {
      return _reader.GetFileCount();
   }

   /**
    * Returns if there are more tokens to read.
    * 
    * @return true if another token can be reader, false otherwise
    */
   public boolean hasMoreToken() {
      return _reader.hasMoreToken();
   }

   /**
    * This method is being called from the CorpusReader everytime a new document
    * of the collection is opened. As soon as a token is read, the
    * getNextToken() method checks if the flag newDoc is set. If that is the
    * case, the current token count is added to the average document length and
    * the token count is set back to 0.
    */
   public void notifyNewDoc() {
      newDoc = true;
   }

   /**
    * This method returns the average document length of the collection.
    * 
    * @return the average token count for a document in the collection
    */
   public double getAvgDocLength() {
      return avgDocLength;
   }

   /**
    * This method returns the HashMap of all token counts for all documents.
    * 
    * @return The HashMap of the token counts for each document
    */
   public HashMap<String, Integer> getDocLengths() {
      return docLengths;
   }
}
