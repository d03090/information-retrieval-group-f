package indexer;

/**
 * This class implements the data structure of a term as a part of a dictionary
 * (block). A term consists of the actual term value and the PostingsList which
 * contains information about the documents in which the term occurs.
 * 
 * @author Leitner Thomas
 * @author Stangl Stefan
 * 
 */
public class Term {
   /**
    * The term value
    */
   private String       term;        // the actual term
   /**
    * The PostingsList corresponding to the term. It contains all documents in
    * which the term occurs and the respective term frequencies.
    */
   private PostingsList postingsList; // the corresponding postingsList

   /**
    * @return the term
    */
   public String getTerm() {
      return term;
   }

   /**
    * @param term
    *           the term to set
    */
   public void setTerm(String term) {
      this.term = term;
   }

   /**
    * @return the postingsList
    */
   public PostingsList getPostingsList() {
      return postingsList;
   }

   /**
    * @param postingsList
    *           the postingsList to set
    */
   public void setPostingsList(PostingsList postingsList) {
      this.postingsList = postingsList;
   }

}
