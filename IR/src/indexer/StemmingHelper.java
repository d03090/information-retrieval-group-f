package indexer;

import org.tartarus.snowball.SnowballStemmer;

/**
 * This class provides some kind of interface for the application programmers to
 * access the Snowball stemmer. The only relevant method is getStemmedTerm which
 * returns the stemmed form of a given term.
 * 
 * @author Leitner Thomas
 * @author Stangl Stefan
 * 
 */
public class StemmingHelper {

   /**
    * The singleton instance.
    */
   private static StemmingHelper _instance = null;
   /**
    * This is needed to get the actual Stemmer class
    */
   @SuppressWarnings("rawtypes")
   private Class                 stemClass;
   /**
    * The actual stemmer class of which the stem() method will be called
    */
   private SnowballStemmer       stemmer;

   /**
    * This static method returns the singleton instance of the class. If it
    * hasn't been initialized, it is instantiated by calling the private
    * constructor of the class.
    * 
    * @return the singleton object
    */
   public static StemmingHelper Instance() {
      if (_instance == null) {
         _instance = new StemmingHelper();
      }

      return _instance;
   }

   /**
    * An instance of the EnglishStemmer class is created. Could be any kind of
    * stemmer at this point.
    */
   private StemmingHelper() {
      try {
         stemClass = Class.forName("org.tartarus.snowball.ext.englishStemmer");

         stemmer = (SnowballStemmer) stemClass.newInstance();
      } catch (ClassNotFoundException e) {
         e.printStackTrace();
      } catch (InstantiationException e) {
         e.printStackTrace();
      } catch (IllegalAccessException e) {
         e.printStackTrace();
      }
   }

   /**
    * The stemmed form of the given term is returned by this method.
    * 
    * @param term
    *           The term of which the stemmed form is returned
    * @return the stemmed form of the term
    */
   public String getStemmedTerm(String term) {
      stemmer.setCurrent(term);
      stemmer.stem();

      return stemmer.getCurrent();
   }
}
