package indexer;

/**
 * This class implements the data structure for storing a single token which is
 * delivered by TokenStreams getNextToken() Method. It consists of the
 * normalized token value, the name of the document in which it was found and
 * the class name of the document.
 * 
 * @author Leitner Thomas
 * @author Stangl Stefan
 * 
 */
public class Token {
   /**
    * The normalized token value. This equals to the value of the corresponding
    * term.
    */
   private String value;
   /**
    * The unique name of the document in which the token was found.
    */
   private String documentName;
   /**
    * The class name of the document in which the token was found.
    */
   private String className;

   /**
    * @return the value
    */
   public String getValue() {
      return value;
   }

   /**
    * @param value
    *           the value to set
    */
   public void setValue(String value) {
      this.value = value;
   }

   /**
    * @return the documentName
    */
   public String getDocumentName() {
      return documentName;
   }

   /**
    * @param documentName
    *           the documentName to set
    */
   public void setDocumentName(String documentName) {
      this.documentName = documentName;
   }

   /**
    * @return the className
    */
   public String getClassName() {
      return className;
   }

   /**
    * @param className
    *           the className to set
    */
   public void setClassName(String className) {
      this.className = className;
   }
}
