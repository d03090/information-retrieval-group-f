package indexer;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import exceptions.RootFolderNotFoundException;

/**
 * This class implements the reader that can read one character at at time from
 * the document collection. The CorpusReader maintains a list of all documents
 * to read from and reads sequentially through all of them. When a file is
 * finished the CorpusReader closes the corresponding stream and opens a new
 * stream for the next file, if there is one.
 * 
 * @author Leitner Thomas
 * @author Stangl Stefan
 * 
 */
public class CorpusReader {
   /**
    * The path to the root folder of the document collection.
    */
   private String       _rootFolder     = null;
   /**
    * The list of all file paths for all documents.
    */
   private List<String> _files          = null;
   /**
    * The altogether document count N.
    */
   private long         _fileCount      = -1L;
   /**
    * The File Object of the document that is currently being read from.
    */
   private File         _currentFile    = null;
   /**
    * The FileReader of the document that is currently being read from.
    */
   private FileReader   _reader         = null;

   /**
    * A reference to the TokenStream so that the notifyNewDoc-method of the
    * TokenStream can be called.
    */
   private TokenStream  _refTokenStream = null;

   /**
    * This field stores the name of the document that has been read before the
    * currently read document
    */
   private String       _previousFile   = null;

   /**
    * The constructor initializes the list of all documents by calling the
    * recursive listFilesForFolder() method by recursively listing all files
    * starting from the rootFolder. _currentFile and _reader always point to the
    * document that is being read from currently.
    * 
    * @param rootFolder
    *           the root folder of the document collection
    * @throws IOException
    * @throws RootFolderNotFoundException
    */
   public CorpusReader(String rootFolder, TokenStream ref) throws IOException,
         RootFolderNotFoundException {
      _rootFolder = rootFolder;
      _files = new ArrayList<String>();

      _refTokenStream = ref;

      File folder = new File(_rootFolder);
      if (folder == null || !folder.exists()) {
         throw new RootFolderNotFoundException("Folder does not exist.");
      }

      listFilesForFolder(new File(_rootFolder));
      _fileCount = _files.size();

      _currentFile = new File(_files.get(0));
      _files.remove(0);
      _reader = new FileReader(_currentFile);
   }

   /**
    * Recursively traverses all Directories under folder and adds all found
    * files to the _files list
    * 
    * @param folder
    *           the root folder for traversing
    * @throws IOException
    */
   private void listFilesForFolder(File folder) throws IOException {
      for (File fileEntry : folder.listFiles()) {
         if (fileEntry.isDirectory()) {
            listFilesForFolder(fileEntry);
         } else {
            _files.add(fileEntry.getCanonicalPath());
         }
      }
   }

   /**
    * This method reads the next character from the current file. If -1 is read,
    * the current FileReader is closed and the reference is set to null. When
    * the method is called the next time, _reader is set to the next available
    * document. If there aren't any documents left, -1 is returned
    * 
    * @return the next character from the document collection, -1 if end of
    *         collection is reached
    * @throws IOException
    */
   public int read() throws IOException {
      // if the reader has been closed in the previous call of read(), open
      // the next file, if any files are left
      if (_reader == null) {
         _refTokenStream.notifyNewDoc(); // also now is the right time to
                                         // send the newDoc-notification
         _previousFile = getDocName();
         if (_files.size() > 0) {
            _currentFile = new File(_files.get(0));
            _files.remove(0);
            _reader = new FileReader(_currentFile);
         } else {
            return -1;
         }
      }

      // read next character
      int i = _reader.read();

      // if -1 was read, close the reader and return ' '
      if (i == -1) {
         _reader.close();
         _reader = null;
         return ' '; // return ' ' to mark the end of any possibly unfinished
                     // token to the TokenStream
      }

      return i;

   }

   /**
    * Returns the unique document name of the current document.
    * 
    * @return document name of the current document
    */
   public String getDocName() {
      return _currentFile.getParentFile().getName() + "/"
            + _currentFile.getName();
   }

   /**
    * Returns the class name of the current document
    * 
    * @return class name of the current document
    */
   public String getClassName() {
      return _currentFile.getParentFile().getName();
   }

   /**
    * Returns the total number N of documents in the collection.
    * 
    * @return the total number N of documents in the collection
    */
   public long GetFileCount() {
      return _fileCount;
   }

   /**
    * Returns if there are more characters to read from the collection
    * 
    * @return true if there are more characters to read, false otherwise
    */
   public boolean hasMoreToken() {
      return (_reader != null);
   }

   /**
    * This method returns the document name of the previously read document
    * 
    * @return the documetn name of the previously read document
    */
   public String getPreviousFile() {
      return _previousFile;
   }
}
