package indexer;

/**
 * This class implements the data structure for an element of the PostingsList.
 * It consists of the documentName and the term frequency. As a PostingsList is
 * always linked to a specific term, the term frequency is always an attribute
 * of a specific (term, documentName) pair.
 * 
 * @author Leitner Thomas
 * @author Stangl Stefan
 * 
 */
public class Posting implements Comparable<Posting> {
   public Posting(String documentName, long termFrequency) {
      this.documentName = documentName;
      this.termFrequency = termFrequency;
   }

   /**
    * The unique name of the document
    */
   private String documentName;
   /**
    * The term frequency is the number of occurrences of the corresponding term
    * in the document
    */
   private long   termFrequency;

   /**
    * Adds 1 to the current term frequency
    */
   public void IncrementTermFrequency() {
      termFrequency++;
   }

   public String getDocumentName() {
      return this.documentName;
   }

   public long getTermFrequency() {
      return this.termFrequency;
   }

   /**
    * Increases the current term frequency by the given amount
    * 
    * @param amount the amount of which the term frequency is increased
    */
   public void increaseTermFrequency(long amount) {
      this.termFrequency += amount;
   }

   /*
    * (non-Javadoc)
    * 
    * @see java.lang.Object#equals(java.lang.Object)
    */
   @Override
   public boolean equals(Object obj) {
      return this.documentName.equals(((Posting) obj).documentName);
   }

   @Override
   public int compareTo(Posting o) {
      return this.documentName.compareTo(o.documentName);
   }
}
