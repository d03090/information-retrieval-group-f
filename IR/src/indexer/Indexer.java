package indexer;

import java.io.*;
import java.util.*;
import java.util.regex.Pattern;
import java.util.zip.GZIPOutputStream;

import main.Config;

import exceptions.RootFolderNotFoundException;

/**
 * This class implements the Indexer of Task1. The indexing process is split
 * into two parts. The entry method is CreateIndex() in which the two methods
 * BuildIndex and WriteArffFile are called. BuildIndex reads all tokens from the
 * TokenStream and writes several block files to disk. The block files are then
 * merged into one big file. For this process the SPIMI algorithm is used.
 * WriteArffFile then reads in the big block file and outputs the resulting arff
 * file.
 * 
 * @author Leitner Thomas
 * @author Stangl Stefan
 * 
 */
public class Indexer {
   /**
    * minimum occurrences of a term in the corpus
    */
   private int                      lowerThreshold;
   /**
    * maximum occurrences of a term in the corpus
    */
   private int                      upperThreshold;
   /**
    * total number of documents
    */
   private long                     N            = 0;
   /**
    * the tokenStream to read one token at a time from
    */
   private TokenStream              tokenStream;
   /**
    * the list of the temporary block files
    */
   private List<String>             tmpFiles;
   /**
    * the current index for the temporary files
    */
   private int                      fileIndex;

   /**
    * This field is used for showing the user some progress when merging the
    * blocks
    */
   private int                      blockLineSum = 0;

   /**
    * The average document length (token count) for the entire collection
    */
   private double                   avgDocLength = 0d;

   /**
    * This field stores the value for the k1 and k3 parameter. So the values for
    * k1 and k3 must always be the same in this implementation.
    */
   private double                   k            = 1.2d;
   /**
    * This field stores the value for the b-parameter. The b-parameter is used
    * to scale the term-weights by document length
    */
   private double                   b            = 0.75d;

   /**
    * HashMap that contains the token count for each document of the collection
    */
   private HashMap<String, Integer> docLengths   = null;

   public Indexer() {

   }

   /**
    * The entry method of the Indexer. The indexing process is split into two
    * parts. At first BuildIndex is called and the merged.tmp file is created.
    * For this step the SPIMI algorithm is used. After this, the merged.tmp file
    * is converted into the resulting arff file, which may or may not be
    * compressed using gzip.
    * 
    * @param rootFolder
    *           the root folder of the collection
    * @param lowerThreshold
    *           lower bound for collection frequency
    * @param upperThreshold
    *           upper bound for collection frequency
    * @param size
    *           name of the resulting arff file, should be small, medium or
    *           large
    * @param useGZIP
    *           flag if the resulting arff file should be compressed using gzip
    */
   public void CreateIndex(String rootFolder, int lowerThreshold,
         int upperThreshold, String size, boolean useGZIP) {
      this.lowerThreshold = lowerThreshold;
      this.upperThreshold = upperThreshold;

      try {
         BuildIndex(rootFolder); // creates merged.tmp using the SPIMI
         // algorithm
         WriteArffFile(size, useGZIP); // reads in merged.tmp and outputs the
         // arff file
      } catch (RootFolderNotFoundException e) {
         System.out
               .println("ERROR: The root folder of the document collection doesn't exist!"
                     + " Please make sure that a folder called \"20_newsgroups_subset\""
                     + " exists in the same directory that the jar file is located in and"
                     + " re-run the application!");
      } catch (IOException e) {
         e.printStackTrace();
      }

   }

   /**
    * This method is the implementation of the SPIMI algorithm. It creates
    * several blocks of data and writes them to temporary files on disk. After
    * that, all the temporary files are merged into one big file called
    * merged.tmp. To process one block of data the method processBlock is
    * called. The method mergeBlocks then merges all tmp files into merged.tmp
    * 
    * @param rootFolder
    *           the root folder of the document collection
    * @throws RootFolderNotFoundException
    * @throws IOException
    */
   private void BuildIndex(String rootFolder)
         throws RootFolderNotFoundException, IOException {
      tokenStream = new TokenStream(rootFolder);
      tmpFiles = new ArrayList<String>();
      fileIndex = 0;
      N = tokenStream.GetFileCount();
      System.out
            .println("Building index for a total of " + N + " documents...");

      // delete the directory for the temporary files
      cleanupTmpDir();

      System.out.println("Creating temporary block files...");

      // iterate through the blocks as long as there are tokens to read
      while (tokenStream.hasMoreToken()) {
         tmpFiles.add(processBlock(tokenStream, fileIndex));
         fileIndex++;
         System.gc(); // after one block has successfully been written to
                      // disk, make a call to the garbage collector
      }

      // store the avgDocLength and all doc-lengths into private fields
      avgDocLength = tokenStream.getAvgDocLength();
      docLengths = tokenStream.getDocLengths();
      System.out.println("Average Document length: " + avgDocLength
            + " tokens!");

      // merge the temporary files into one big file
      mergeBlocks(tmpFiles);

      // the temporary files could be deleted here, but it's not
      // indispensable, so we don't delete them. Also, maybe you want to take a
      // look at them

      System.out.println("Finished merging blocks!");
   }

   /**
    * This method reads tokens from the TokenStream until there isn't any
    * simulated memory left. To achieve this, for every token an amount of bytes
    * is assumed to be needed to store the information in memory. So as every
    * token is read the simulated memory usage is increased by that amount. When
    * the maximum Available Memory is reached, the block is finished and written
    * to disk. SizePerToken and AvailableMemory are constants in the main.Config
    * class. The simulation of memory usage has been implemented to achieve some
    * kind of scalability in this part of indexing.
    * 
    * @param tokenStream
    *           The Stream of Tokens
    * @param fileIndex
    *           The current index of the tmp output file
    * @return the filename of the temporary block
    * @throws IOException
    */
   private String processBlock(TokenStream tokenStream, int fileIndex)
         throws IOException {
      Dictionary dictionary = new Dictionary();
      Token token = null;
      TreeMap<String, PostingsList> map = dictionary.GetDictionary();
      long blockSize = 0; // in byte
      int i = 0;
      int nextStep = 10;

      System.out.print("Writing temporary block file " + fileIndex + ".tmp");

      // while there's still memory left and the tokenStream is not empty read
      // another token
      while ((blockSize + Config.SizePerToken) <= (Config.AvailableMemory * 1024 * 1024)
            && (token = tokenStream.getNextToken()) != null) {

         // add the token to the dictionary, either add new term or add
         // posting
         // to existing terms postingsList
         if (map.containsKey(token.getValue())) {

            PostingsList postings = map.get(token.getValue());
            postings.addPosting(token.getDocumentName());

         } else {

            PostingsList postings = new PostingsList();
            postings.add(new Posting(token.getDocumentName(), 1));

            map.put(token.getValue(), postings);
         }

         blockSize += Config.SizePerToken;
         i++;
         if (i % 1000 == 0) {
            // this is just some fancy output to give more information about
            // the
            // progress
            if (((blockSize * 100L) / (long) (Config.AvailableMemory * 1024 * 1024)) > nextStep) {
               System.out.print(".");
               nextStep += 10;
            }
         }
      }

      blockLineSum += map.size();
      String fileName = Config.TmpDir + "/" + fileIndex + ".tmp";
      PrintWriter out = new PrintWriter(fileName);

      // write the Dictionary onto disk, one term per line
      // Format: term df doc1:tf1|doc2:tf2|...|docn:tfn|
      for (Map.Entry<String, PostingsList> entry : map.entrySet()) {
         out.print(entry.getKey() + " " + entry.getValue().size() + " ");
         for (Posting p : entry.getValue()) {
            out.print(p.getDocumentName() + ":" + p.getTermFrequency() + "|");
         }
         out.println();
      }

      out.close();

      System.out.println("done");
      return fileName;
   }

   /**
    * This method deletes all files in the directory for the temporary block
    * files and the directory itself
    * 
    * @throws IOException
    */
   private void cleanupTmpDir() throws IOException {
      File tmpDir = new File(Config.TmpDir);
      if (tmpDir.exists() && tmpDir.isDirectory())
         delete(tmpDir);
      tmpDir = new File(Config.TmpDir);
      if (!tmpDir.mkdir()) {
         try {
            Thread.sleep(1000); // in Windows 7 the tmpDir could not be
            // created on the first try, when the user
            // had
            // the
            // dir opened in explorer before it was
            // deleted
         } catch (InterruptedException e) {
            e.printStackTrace();
         }
         if (!tmpDir.mkdir())
            throw new IOException(
                  "Cannot create directory for temporary files!");
      }
   }

   /**
    * This method deletes the File f. If f is a directory, the method is called
    * recursively for all files and directories in f
    * 
    * @param f
    *           the file or directory to be deleted
    * @throws FileNotFoundException
    */
   private void delete(File f) throws FileNotFoundException {
      if (f.isDirectory()) {
         for (File s : f.listFiles()) {
            delete(s);
         }
      }
      if (!f.delete())
         throw new FileNotFoundException("Could not delete file " + f.getName());
   }

   /**
    * This method merges all created temporary block files into the merged.tmp
    * file. For every input file one BufferedReader is opened. The terms in
    * every tmp file are sorted in ascending order, so they can easily be merged
    * into one big merged.tmp file. Every reader reads one term into memory.
    * Then from all read terms the smallest one is selected and written into
    * merged.tmp. If there are several occurrences of this term, they are
    * merged. If one term is processed this way, the corresponding reader reads
    * in the next term from the file. This process repeats until all readers are
    * closed.
    * 
    * @param files
    *           the list of paths to the tmp block files to be merged
    * @throws IOException
    */
   private void mergeBlocks(List<String> files) throws IOException {
      // the hashmap stores for every reader the current term
      LinkedHashMap<BufferedReader, Term> readers = new LinkedHashMap<BufferedReader, Term>();
      List<BufferedReader> toRemove = new ArrayList<BufferedReader>();
      PrintWriter out = new PrintWriter(Config.TmpDir + "/merged.tmp");
      String line = "";
      Term outTerm = null;
      long readLinesSum = 0;
      int nextStep = 10;
      int i = 0;

      System.out.print("Merging block files into merged.tmp");

      // open readers and create Term-Lists
      for (String file : files) {
         readers.put(new BufferedReader(new FileReader(file)), null);
      }

      // read first lines
      for (Map.Entry<BufferedReader, Term> entry : readers.entrySet()) {
         if ((line = entry.getKey().readLine()) != null) {
            entry.setValue(parseLine(line));
            readLinesSum++;
         } else {
            // closing the reader
            entry.getKey().close();
            toRemove.add(entry.getKey());
         }
      }
      for (BufferedReader br : toRemove) {
         readers.remove(br);
      }
      toRemove.clear();

      // read all remaining lines
      while (readers.size() > 0) {
         for (Map.Entry<BufferedReader, Term> entry : readers.entrySet()) {
            // if the current term for the reader is null, the next term
            // must be
            // read
            if (entry.getValue() == null) {
               if ((line = entry.getKey().readLine()) != null) {
                  entry.setValue(parseLine(line));
                  readLinesSum++;
               } else {
                  // closing the reader
                  entry.getKey().close();
                  toRemove.add(entry.getKey());
               }
            }
         }
         for (BufferedReader br : toRemove) {
            readers.remove(br);
         }
         toRemove.clear();

         // find the smallest term(s), optionally merge them and then remove
         // them from the hashmap
         if (readers.size() > 0) {
            outTerm = mergeTerms(readers);
            if (outTerm != null) {
               out.print(outTerm.getTerm() + " "
                     + outTerm.getPostingsList().size() + " ");
               for (Posting p : outTerm.getPostingsList()) {
                  out.print(p.getDocumentName() + ":" + p.getTermFrequency()
                        + "|");
               }
               out.println();
            }
         }

         // every 100 loops check if another 10% of the total lines in all
         // tmp
         // files have been read. if so, print out a dot to show the user
         // some
         // progress
         i++;
         if (i % 100 == 0) {
            if (((readLinesSum * 100L) / (long) blockLineSum) > nextStep) {
               System.out.print(".");
               nextStep += 10;
            }
         }
      }
      out.close();
      System.out.println("done");
   }

   /**
    * This method reads in one line from a temporary block file and parses it
    * into a Term-Object. The line is of the following format:
    * "term docFrequ doc1:termFrequ1|doc2:termFrequ2|"
    * 
    * @param line
    *           the line in the tmp file to be parsed in the Term-Object
    * @return the Term-Object
    */
   private Term parseLine(String line) {
      // term docFrequ doc1:termFrequ1|doc2:termFrequ2|
      if (line != null && !line.trim().equals("")) {
         Term ret = new Term();
         String[] parts = line.split(" ");
         String term = parts[0];
         ret.setTerm(term);
         String[] postings = parts[2].split(Pattern.quote("|"));
         PostingsList postingsList = new PostingsList();
         for (String posting : postings) {
            if (posting != null && !posting.equals("")) {
               int tf = Integer.parseInt(posting.split(Pattern.quote(":"))[1]);
               postingsList.add(new Posting(
                     posting.split(Pattern.quote(":"))[0], tf));
            }
         }
         ret.setPostingsList(postingsList);
         return ret;
      } else {
         return null;
      }
   }

   /**
    * This method gets the current terms of all remaining open readers and picks
    * the smallest term of them. If this terms occurs in several readers, the
    * postings Lists are merged.
    * 
    * @param readers
    *           the readers for all remaining block files together with their
    *           current terms
    * @return the merged term
    */
   private Term mergeTerms(LinkedHashMap<BufferedReader, Term> readers) {
      LinkedHashMap<BufferedReader, Term> processedTerms = new LinkedHashMap<BufferedReader, Term>();

      // iterate through all readers and find the smallest terms
      // put the smallest terms into processedTerms
      for (Map.Entry<BufferedReader, Term> entry : readers.entrySet()) {
         if (processedTerms.size() == 0) {
            processedTerms.put(entry.getKey(), entry.getValue());
         } else if (entry
               .getValue()
               .getTerm()
               .compareTo(
                     processedTerms.entrySet().iterator().next().getValue()
                           .getTerm()) < 0) {
            processedTerms.clear();
            processedTerms.put(entry.getKey(), entry.getValue());
         } else if (entry
               .getValue()
               .getTerm()
               .equals(
                     processedTerms.entrySet().iterator().next().getValue()
                           .getTerm())) {
            processedTerms.put(entry.getKey(), entry.getValue());
         }
      }

      // now iterate through processedTerms and eventually merge postings
      // lists
      Term term = null;
      for (Map.Entry<BufferedReader, Term> entry : processedTerms.entrySet()) {
         if (term == null) {
            term = entry.getValue();
         } else { // now merge the postings lists
            if (!term.getTerm().equals(entry.getValue().getTerm())) {
               System.err.println("FATAL! Error that should not happen!");
               System.exit(1);
            }
            term.getPostingsList().merge(entry.getValue().getPostingsList());
         }
         readers.put(entry.getKey(), null);
      }

      // finally we have to calculate the collection frequency of the term and
      // if it is beyond one of the thresholds, ignore the term
      int tfForAllDocs = 0;
      for (Posting p : term.getPostingsList()) {
         tfForAllDocs += p.getTermFrequency();
      }

      if (tfForAllDocs < lowerThreshold || tfForAllDocs > upperThreshold) {
         term = null;
      }

      return term;
   }

   /**
    * This method reads in the merged.tmp file in which terms are stored line by
    * line and stores the information as an arff file in which the documents are
    * stored line by line.<br />
    * To achieve this the whole content of merged.tmp has to be read into
    * memory. So this part of the indexing process unfortunately doesn't scale.
    * The alternative would have been to parse through the merged.tmp file one
    * time for each document, so for N=8000 docs, we would have to go through
    * the file a total of 8000 times.<br />
    * So there was a RAM usage / processing time trade off and we chose to fill
    * up the RAM.<br />
    * More detailed description: At first the whole merged.tmp is read into an
    * ArrayList of StringBuilder objects in which every StringBuilder represents
    * one line of the file. Then, for every document the whole ArrayList is
    * traversed and all terms for the smallest document are collected. The
    * information then is deleted from the ArrayList and the process is
    * continued with the next document until the ArrayList is empty.
    * 
    * @param size
    *           Defines the file name of the output arff file. Should generally
    *           be either small, medium or large.
    * @param useGZIP
    *           true if and only if the gzip compression algorithm should be
    *           used to compress the output arff file, false otherwise
    */
   private void WriteArffFile(String size, boolean useGZIP) {
      try {
         BufferedReader br = new BufferedReader(new FileReader(Config.TmpDir
               + "/merged.tmp"));
         List<StringBuilder> file = new ArrayList<StringBuilder>();
         String currentDocument = "";

         ArffData data = null;
         StringBuilder termsCSV = new StringBuilder();
         int df, tf;
         List<Integer> lineNumbers = new ArrayList<Integer>();
         int i = 0;
         double weight;

         PrintWriter arffWriter = null;

         if (useGZIP) {
            arffWriter = new PrintWriter(new OutputStreamWriter(
                  new GZIPOutputStream(new FileOutputStream(new File(size
                        + ".arff.gz")))));
         } else {
            arffWriter = new PrintWriter(new File(size + ".arff"));
         }

         arffWriter.println("% Thresholds: " + lowerThreshold + " to "
               + upperThreshold);
         arffWriter.println("@RELATION \"newsgroups " + size + "\"");
         arffWriter.println();

         // documentName,className,weightTerm1,weightTerm2,...,weightTermM
         arffWriter.println("@ATTRIBUTE docName STRING");
         arffWriter.println("@ATTRIBUTE className STRING");

         String line;
         while ((line = br.readLine()) != null) {
            file.add(new StringBuilder(line));
            termsCSV.append(";" + line.split(Pattern.quote(" "))[0]);
            arffWriter.println("@ATTRIBUTE term_"
                  + line.split(Pattern.quote(" "))[0] + " STRING");
         }
         br.close();

         arffWriter.println();
         arffWriter.println("@DATA");

         // we need all terms in ascending order for later processing
         String[] terms = termsCSV.substring(1).split(Pattern.quote(";"));

         System.gc();

         // the outer while loop has N iterations when N is the number of
         // documents still contained in merged.tmp
         while (file.size() > 0) {
            i = 0;
            currentDocument = "";

            // now iterate through the ArrayList file, find all terms for
            // the smallest document and store them in an ArffData object
            for (StringBuilder sb : file) {

               String document = sb.substring(sb.lastIndexOf(" ") + 1,
                     sb.indexOf(":"));
               if (currentDocument.equals("")
                     || document.compareTo(currentDocument) < 0) {
                  // now we have found a new smallest document
                  currentDocument = document;
                  data = new ArffData();
                  data.setDocumentName(document);
                  data.setClassName(document.substring(0, document.indexOf('/')));
                  df = Integer.parseInt(sb.substring(sb.indexOf(" ") + 1,
                        sb.lastIndexOf(" ")));
                  tf = Integer.parseInt(sb.substring(sb.indexOf(":") + 1,
                        sb.indexOf("|")));
                  // 300 initial capacity has proven to be quite efficient
                  data.setTermWeights(new HashMap<String, Double>(300));
                  data.getTermWeights().put(sb.substring(0, sb.indexOf(" ")),
                        calcBM25Weight(df, tf, docLengths.get(document)));
                  lineNumbers.clear();
                  lineNumbers.add(i);
               } else if (document.equals(currentDocument)) {
                  // now we have found a new occurrence of the already smallest
                  // document
                  df = Integer.parseInt(sb.substring(sb.indexOf(" ") + 1,
                        sb.lastIndexOf(" ")));
                  tf = Integer.parseInt(sb.substring(sb.indexOf(":") + 1,
                        sb.indexOf("|")));
                  data.getTermWeights().put(sb.substring(0, sb.indexOf(" ")),
                        calcBM25Weight(df, tf, docLengths.get(document)));
                  lineNumbers.add(i);
               }
               i++;
            }

            // now all entries in the ArrayList file have to be adjusted as
            // the found Document has to be erased
            for (int k = lineNumbers.size() - 1; k >= 0; k--) {
               int j = lineNumbers.get(k);
               StringBuilder lineToAlter = file.get(j);
               lineToAlter.delete(lineToAlter.lastIndexOf(" ") + 1,
                     lineToAlter.indexOf("|") + 1);

               if (lineToAlter.indexOf("|") == -1) {
                  file.remove(j);
               }
            }
            lineNumbers.clear();

            // write first 2 data fields for the document
            arffWriter.print(data.getDocumentName() + "," + data.getClassName()
                  + ",");

            // then build the rest of the arff line (the term weights for
            // the doc) and write it out
            StringBuilder lineToWrite = new StringBuilder();
            for (int t = 0; t < terms.length; t++) {
               weight = 0d;

               if (data.getTermWeights().containsKey(terms[t])) {
                  weight = data.getTermWeights().get(terms[t]);
               }

               lineToWrite.append(weight);

               if (t < terms.length - 1) {
                  lineToWrite.append(",");
               }
            }
            arffWriter.print(lineToWrite.toString());

            System.out.println("Document: " + currentDocument
                  + ", remaining terms: " + file.size());

            arffWriter.println();
         }

         arffWriter.close();

      } catch (Exception ex) {
         ex.printStackTrace();
      }
   }

   /**
    * This method calculates the weight for one (DocId, Term) pair. The basic
    * parameters used are the document-frequency of the term and the
    * term-frequency of the (DocId, Term) pair. The logarithmic formula for the
    * calculation was mentioned in the course and is also stated in the book
    * "Information Retrieval".
    * 
    * @param df
    *           the document-frequency - the number of documents in which the
    *           term occurs
    * @param tf
    *           the term-frequency - the number of occurrences of the term in
    *           the document
    * @return the weight for the (DocId, Term) pair
    */
   @SuppressWarnings("unused")
   @Deprecated
   private double calcWeight(int df, int tf) {
      return Math.log10(1d + (double) tf)
            * Math.log10((double) N / (double) df);
   }

   /**
    * This method calculates the BM25-weight for one (DocId, Term) pair. The
    * parameters used in the formula are the following: <br />
    * N: altogether document count in the collection <br />
    * df: document frequency of the term <br />
    * tf: term frequency of the term in the document <br />
    * ld: document length (token count) of the document <br />
    * b: tuning parameter that controls scaling by doc-length <br />
    * k: tuning parameter that controls scaling by term-frequency
    * 
    * @param df
    *           document frequency of the term
    * @param tf
    *           term frequency of the term in the document
    * @param ld
    *           document length (token count) of the document
    * @return the BM25 weight for a (Doc, Term) pair
    */
   private double calcBM25Weight(int df, int tf, int ld) {
      double ddf = (double) df;
      double dtf = (double) tf;
      double dld = (double) ld;

      // The Square-Root of the idf is used so that, when two weights are
      // multiplied to calculate the rsv of two documents, the original idf is
      // used. That is the case because Sqrt(x)*Sqrt(x) == x.
      return Math.sqrt(Math.log10((double) N / ddf))
            * (((k + 1d) * dtf) / (k * ((1d - b) + b * (dld / avgDocLength)) + dtf));
   }
}
