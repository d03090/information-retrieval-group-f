package exceptions;

/**
 * This exception is thrown, if the provided root-folder for the document
 * collection doesn't exist
 * 
 * @author Leitner Thomas
 * @author Stangl Stefan
 * 
 */
public class RootFolderNotFoundException extends Exception {

   private static final long serialVersionUID = 1L;

   public RootFolderNotFoundException(String string) {
   }

}
