package similarityEngine;

import java.util.HashMap;
import java.util.Map;

/**
 * This class implements the data structure that is needed to represent a vector
 * containing the weights for each term in a document. One document vector
 * corresponds to one line in the ARFF file.
 * 
 * @author Leitner Thomas
 * @author Stangl Stefan
 * 
 */
public class DocumentVector {
   /**
    * The unique name of the document.
    */
   private String                   docName;
   /**
    * The name of the class of the document.
    */
   private String                   docClass;
   /**
    * The weights for each term. Weights with the value 0.0 will not be stored.
    */
   private HashMap<Integer, Double> weights;
   /**
    * The cosine distance from the current document vector to the document
    * vector of the searching document.
    */
   @Deprecated
   private double                   cosine;

   /**
    * The rsv (retrieval status value) of the document. It is computed by adding
    * together all term weights of terms that appear in the document AND in the
    * corresponding reference-document.
    */
   private double                   rsv;

   /**
    * @return the docName
    */
   public String getDocName() {
      return docName;
   }

   /**
    * @param docName
    *           the docName to set
    */
   public void setDocName(String docName) {
      this.docName = docName;
   }

   /**
    * @return the docClass
    */
   public String getDocClass() {
      return docClass;
   }

   /**
    * @param docClass
    *           the docClass to set
    */
   public void setDocClass(String docClass) {
      this.docClass = docClass;
   }

   /**
    * @return the weights
    */
   public HashMap<Integer, Double> getWeights() {
      return weights;
   }

   /**
    * @param weights
    *           the weights to set
    */
   public void setWeights(HashMap<Integer, Double> weights) {
      this.weights = weights;
   }

   /**
    * @return the cosine distance
    */
   @Deprecated
   public double getCosine() {
      return cosine;
   }

   /**
    * 
    * @param cosine
    *           the cosine distance to set
    */
   @Deprecated
   public void setCosine(double cosine) {
      this.cosine = cosine;
   }

   /**
    * @return the length of the document vector that's the square root of the
    *         sum of the vector components squared
    */
   @Deprecated
   public double getLength() {
      double ret = 0d;

      for (Map.Entry<Integer, Double> entry : weights.entrySet()) {
         ret += Math.pow(entry.getValue(), 2);
      }

      ret = Math.sqrt(ret);

      return ret;
   }

   /**
    * returns the rsv of the document vector
    * 
    * @return the retrieval status value of the document
    */
   public double getRsv() {
      return rsv;
   }

   /**
    * sets the rsv of the document vector
    * 
    * @param rsv the rsv of the document
    */
   public void setRsv(double rsv) {
      this.rsv = rsv;
   }

   /**
    * Calculated the dot product of two vectors (a.b)
    * 
    * @param a
    *           vector a
    * @param b
    *           vector b
    * @return the dot product of a and b
    */
   public static double DotProduct(DocumentVector a, DocumentVector b) {
      double ret = 0d;

      if (b.weights.size() < a.weights.size()) {
         DocumentVector tmp = a;
         a = b;
         b = tmp;
      }

      for (Map.Entry<Integer, Double> entry : a.weights.entrySet()) {
         if (b.weights.containsKey(entry.getKey())) {
            ret += (entry.getValue() * b.weights.get(entry.getKey()));
         }
      }

      return ret;
   }
}
