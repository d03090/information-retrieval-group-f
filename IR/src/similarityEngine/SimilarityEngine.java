package similarityEngine;

import java.io.*;
import java.util.*;
import java.util.zip.GZIPInputStream;

/**
 * This class provides the ability to search for similar documents of a given
 * document.
 * 
 * @author Leitner Thomas
 * @author Stangl Stefan
 * 
 */
public class SimilarityEngine {
   /**
    * caches the document vectors for one index file (small, medium, large)
    */
   List<DocumentVector> _docVectors;

   public SimilarityEngine() {
      _docVectors = null;
   }

   /**
    * Takes the documents from the input file and searches for n similar
    * documents. Using the large index only. The output will be stored in text
    * files for each document and for each used index.
    * 
    * @param inputFile
    *           file containing the document names
    * @param n
    *           the number of how many similar documents that should be found
    */
   public void runSimilaritySearch(String inputFile, int n) {
      // creating the out dir
      File outDir = new File("out");
      if (!outDir.exists() || !outDir.isDirectory()) {
         outDir.mkdir();
      }

      /*
       * BuildDocVectors("small"); FindNSimilarDocs(inputFile, n, "small");
       * BuildDocVectors("medium"); FindNSimilarDocs(inputFile, n, "medium");
       */
      // only use the lare index in Exercise 2
      BuildDocVectors("large");
      FindNSimilarDocs(inputFile, n, "large");
   }

   /**
    * Reads the document vectors from the index file specified with the size
    * parameter and stores them in memory for further use.
    * 
    * @param size
    *           name of the used index (small, medium, large)
    */
   private void BuildDocVectors(String size) {
      System.out.print("Loading " + size
            + " index into memory and generating document vectors ");

      _docVectors = new ArrayList<DocumentVector>();

      // gets the BufferedReader for the index file (*.arff or *.arff.gz)
      BufferedReader reader = getIndexReader(size);
      String[] lineParts;

      String line;
      boolean searchForDocs = false;
      long milliSeconds = System.currentTimeMillis();
      DocumentVector vector;

      try {
         // iterate through all lines
         while ((line = reader.readLine()) != null) {
            // if line contains data
            if (searchForDocs && !line.trim().equals("")
                  && !line.trim().startsWith("%")) {

               lineParts = line.split(",");
               vector = new DocumentVector();
               vector.setDocName(lineParts[0]);
               vector.setDocClass(lineParts[1]);
               vector.setWeights(new HashMap<Integer, Double>());
               // vector.setCosine(1d);
               vector.setRsv(0d);

               // set the weights in the document vector, which are greater than
               // 0.0
               for (int i = 2; i < lineParts.length; i++) {
                  if (!lineParts[i].equals("0.0")) {
                     double d = Double.parseDouble(lineParts[i]);
                     if (d > 0) {
                        vector.getWeights().put(i - 2, d);
                     }
                  }
               }

               _docVectors.add(vector);
            } else {
               if (line.toUpperCase().equals("@DATA")) {
                  searchForDocs = true;
               }
            }
         }
      } catch (NumberFormatException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      } catch (IOException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      System.out.println("Done. Time needed for loading " + size + " index: "
            + (System.currentTimeMillis() - milliSeconds));
   }

   /**
    * Takes the documents from the input file and searches for n similar
    * documents using the index file that is defined by the size parameter. The
    * output will be stored in text files for each document and for each used
    * index.
    * 
    * @param inputFile
    *           file containing the document names
    * @param n
    *           the number of how many similar documents that should be found
    * @param size
    *           name of the used index (small, medium, large)
    */
   private void FindNSimilarDocs(String inputFile, int n, String size) {
      BufferedReader br;
      try {
         br = new BufferedReader(new InputStreamReader(new FileInputStream(
               inputFile)));

         String topic;
         int i = 1;

         long milliSeconds = System.currentTimeMillis();
         // iterate through all documents of the search query (topics)
         while ((topic = br.readLine()) != null) {
            FindNSimilarDocsForSize(topic, n, size, i);
            System.out.println("Time for Topic " + i + "_" + size + ": "
                  + (System.currentTimeMillis() - milliSeconds) + "ms");
            milliSeconds = System.currentTimeMillis();
            i++;
         }
      } catch (FileNotFoundException e) {
         System.out
               .println("ERROR: topics.txt was not found! Please make sure that the file is located"
                     + " in the same directory as the jar file is in and that the file contains one topic per line!");
         System.exit(1);
      } catch (IOException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
   }

   /**
    * Searches for n similar documents for the specified document name using the
    * index, that is specified with the size parameter.
    * 
    * @param documentName
    *           name of the document for that similar documents should be found
    * @param n
    *           the number of how many similar documents that should be found
    * @param size
    *           name of the used index (small, medium, large)
    * @param topicNumber
    *           number of the current topic, for creating the output text files
    */
   private void FindNSimilarDocsForSize(String documentName, int n,
         String size, int topicNumber) {
      DocumentVector searchVector = null;
      List<DocumentVector> topNSimilar = new ArrayList<DocumentVector>(n);
      boolean found = false;

      // iterate through the cached document vectors and searching for the
      // searched document
      for (DocumentVector vector : _docVectors) {
         if (vector.getDocName().equals(documentName)) {
            searchVector = vector;
            found = true;
            break;
         }
      }

      if (found) {
         // now iterate through all documents in the index and keep the
         // top N rsv documents in memory
         for (DocumentVector currentVector : _docVectors) {
            if (currentVector != searchVector) {
               
               currentVector.setRsv(calculateRSV(searchVector, currentVector));

               // update TopNList
               addDocumentVectorToTopNList(currentVector, topNSimilar, n);
            }
         }

         // iterate through the result list and write it to an output text file
         PrintWriter writer;
         try {
            writer = new PrintWriter("out/" + size + "_topic" + topicNumber
                  + "_groupF.txt");

            int i = 1;
            for (DocumentVector v : topNSimilar) {
               writer.println("topic" + topicNumber + " Q0 " + v.getDocName()
                     + " " + i + " " + (v.getRsv()) + " groupF_" + size);

               i++;
            }

            writer.close();
            writer = null;
         } catch (FileNotFoundException e) {
            e.printStackTrace();
         }
      } else {
         PrintWriter writer;
         try {
            writer = new PrintWriter("out/" + size + "_topic" + topicNumber
                  + "_groupF.txt");

            writer.close();
            writer = null;
         } catch (FileNotFoundException e) {
            e.printStackTrace();
         }
         System.out.println("Reference Document \"" + documentName
               + "\" is not a part of the index!");

      }
   }

   /**
    * Calculates the cosine distance between the searchVector and the
    * currentVector. This is the dot product of the two vectors divided by the
    * product of their lengths.
    * 
    * @param searchVector
    *           document vector of the document, for which similar documents
    *           should be found
    * @param currentVector
    *           the current document vector
    * @return the dot product of the two vectors divided by the product of their
    *         lengths.
    */
   @SuppressWarnings("unused")
   @Deprecated
   private double calculateCosine(DocumentVector searchVector,
         DocumentVector currentVector) {
      return DocumentVector.DotProduct(searchVector, currentVector)
            / (searchVector.getLength() * currentVector.getLength());
   }

   /**
    * This method calculates the retrieval status value (rsv) for the document
    * "currentVector" in regards to the reference document "searchVector". The
    * rsv is basically the dot-product of the two vectors. For each term that
    * appears in both documents, the weights are multiplied and then all added
    * together.
    * 
    * @param searchVector
    *           the vector of the reference document (in other words the query)
    * @param currentVector
    *           the vector of the current scanned document
    * @return the rsv of the two documents
    */
   private double calculateRSV(DocumentVector searchVector,
         DocumentVector currentVector) {
      return DocumentVector.DotProduct(searchVector, currentVector);
   }

   /**
    * Adds the current document vector in the topNList if its rsv is
    * greater than at least one entry in the topNList or if the topNList
    * contains less than n document vectors.
    * 
    * @param currentVector
    *           document vector that should be added
    * @param topNList
    *           list of the n most similar document vectors
    * @param n
    *           maximum size of the topNList
    */
   private void addDocumentVectorToTopNList(DocumentVector currentVector,
         List<DocumentVector> topNList, int n) {
      boolean found = false;

      // insert into the topNList in descending order of the rsv
      for (int i = 0; !found && i < topNList.size(); i++) {
         if (topNList.get(i).getRsv() < currentVector.getRsv()) {
            found = true;
            topNList.add(i, currentVector);
         }
      }

      if (!found && topNList.size() < n)
         topNList.add(currentVector);
      else if (topNList.size() > n)
         topNList.remove(topNList.size() - 1);

   }

   /**
    * Get the BufferedReader for the current index file, specified by the size
    * parameter. This should be [size].arff.gz or if that file does not exist
    * [size].arff. If none of these files exist the program will exit and an
    * error message will be printed.
    * 
    * @param size
    *           name of the used index (small, medium, large)
    * @return the BufferedReader for the current index file
    */
   private BufferedReader getIndexReader(String size) {
      BufferedReader reader = null;
      File fIndex = new File(size + ".arff.gz");

      if (fIndex.exists()) {
         try {
            reader = new BufferedReader(new InputStreamReader(
                  new GZIPInputStream(new FileInputStream(
                        fIndex.getAbsolutePath()))));

            System.out.println("using " + fIndex.getName());
         } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
         } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
         }
      } else {
         System.out.println(size + ".arff.gz does not exist! Trying " + size
               + ".arff");

         fIndex = new File(size + ".arff");

         if (fIndex.exists()) {
            try {
               reader = new BufferedReader(new InputStreamReader(
                     new FileInputStream(fIndex.getAbsolutePath())));

               System.out.println("Using " + fIndex.getName());
            } catch (FileNotFoundException e) {
               // TODO Auto-generated catch block
               e.printStackTrace();
            }
         } else {
            System.out.println(size + ".arff does not exist! No " + size
                  + " index exists :(");
            System.out
                  .println("Please run the indexer first. Start the programm with --indexer to create the (large) index. See help.");
            System.exit(1);
         }
      }

      return reader;
   }
}