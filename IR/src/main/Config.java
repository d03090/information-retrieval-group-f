package main;

/**
 * This class contains some configuration parameters for the application. Those
 * parameters are not supposed to be changed by the end-user, but rather by the
 * developers to change the behaviour of the application
 * 
 * @author Leitner Thomas
 * @author Stangl Stefan
 */
public class Config {
   /**
    * This value is used to simulate the available main memory. The indexer will
    * use a given byte-value for one token and will only process so many tokens
    * per one block, that the AvailableMemory won't be exceeded.
    */
   public static final int    AvailableMemory = 64;   // in MB
   /**
    * The indexer will use this value as the number of bytes that are needed in
    * main memory to store one token. This is only an approximation. The
    * concrete number has been approximated from measurements of RAM usage, when
    * processing all tokens in one big block.
    */
   public static final int    SizePerToken    = 70;   // in Byte
   /**
    * The path to the directory, in which the temporary block-files are stored
    * during indexing time.
    */
   public static final String TmpDir          = "tmp"; // relative path of the
                                                       // directory
                                                       // for the temporary
                                                       // intermediate files
}
