package main;

import similarityEngine.SimilarityEngine;
import gnu.getopt.Getopt;
import gnu.getopt.LongOpt;
import indexer.Indexer;

/**
 * This is the main class of the application The given options are discovered
 * with the getopt implementation for java and then either the indexer or the
 * similarity detection engine is called
 * 
 * @author Leitner Thomas
 * @author Stangl Stefan
 */
public class Main {
   /**
    * The reference that is instantiated in the static main method.
    */
   private static Main _main = null;

   private Main() {
   }

   /**
    * This is the entry point of the application. The getopt implementation for
    * Java is used to read in the command line options. After the option
    * processing either the indexer or the similarity search is called. <br />
    * Notice: the version and the upper and lower threshold options aren't
    * needed anymore, because the similarity search of Task 2 only uses the
    * large index that contains all terms. So if the user generates any file
    * other than the large one, it won't ever be used by the search.
    * 
    * @param args
    *           the command line arguments
    */
   public static void main(String[] args) {
      _main = new Main();

      // all variables are initialized with the default values
      boolean runIndexer = false;
      int lower = 0;
      int upper = Integer.MAX_VALUE;
      String version = "large";
      boolean useGZIP = true;

      // the array for the long options
      LongOpt[] longOpts = new LongOpt[] {
            new LongOpt("version", LongOpt.REQUIRED_ARGUMENT, null, 'v'),
            new LongOpt("indexer", LongOpt.NO_ARGUMENT, null, 'i'),
            new LongOpt("help", LongOpt.NO_ARGUMENT, null, 'h'),
            new LongOpt("upper", LongOpt.REQUIRED_ARGUMENT, null, 'u'),
            new LongOpt("lower", LongOpt.REQUIRED_ARGUMENT, null, 'l'),
            new LongOpt("no-compress", LongOpt.NO_ARGUMENT, null, 'n') };
      Getopt g = new Getopt("task1.jar", args, "iv:hu:l:n", longOpts);

      int c;
      while ((c = g.getopt()) != -1) {
         switch (c) {
            case 'i':
               runIndexer = true;
               break;
            case 'u': // not needed for Task 2
               try {
                  upper = Integer.parseInt(g.getOptarg());
               } catch (Exception ex) {
                  printUsage();
                  System.exit(1);
               }
               break;
            case 'l': // not needed for Task 2
               try {
                  lower = Integer.parseInt(g.getOptarg());
               } catch (Exception ex) {
                  printUsage();
                  System.exit(1);
               }
               break;
            case 'v': // not needed for Task 2
               version = g.getOptarg();
               break;
            case 'n':
               useGZIP = false;
               break;
            case 'h':
               printUsage();
               System.out
                     .println("NOTICE: The folder \"20_newsgroups_subset\" is treated as the root"
                           + " folder of the document collection and therefore must exist.\n"
                           + "Also the file \"topics.txt\" contains the topics for the similarity"
                           + " search and must exist!");
               System.out
                     .println("NOTICE: for Task2 only the large index is needed, so it is sufficient"
                           + " to call the indexer without any additional options!");
               System.exit(0);
               break;
            case '?':
               printUsage();
               System.exit(1);
               break;
            default:
               System.out.print("getopt() returned " + c + "\n");
               System.exit(1);
         }
      }

      if (runIndexer) {
         // the indexer is being called
         System.out.println("Creating index ...");
         long start = System.currentTimeMillis();
         _main.runIndexer(lower, upper, version, useGZIP);
         System.out.println("Index created. Seconds: "
               + ((float) (System.currentTimeMillis() - start) / 1000f));
      } else {
         // the similarity detection is being called
         System.out.println("Searching most similar documents ...");
         long start = System.currentTimeMillis();
         _main.runSimilaritySearch();
         System.out.println("Similarity search finished. Seconds: "
               + ((float) (System.currentTimeMillis() - start) / 1000f));
      }

   }

   /**
    * This method is called, when the option --indexer (respectively -i) is
    * provided with the application call. A new Indexer object is instanciated
    * and the CreateIndex method of that object is called.
    * 
    * @param l
    *           the lower bound for the collection frequency, only terms with cf
    *           >= l will be part of the index
    * @param u
    *           the upper bound for the collection frequency, only terms with cf
    *           <= u will be part of the index
    * @param version
    *           can be for example "small", "medium" or "large"
    * @param useGZIP
    *           if true, the arff file will be compressed using gzip
    */
   private void runIndexer(int l, int u, String version, boolean useGZIP) {
      Indexer indexer = new Indexer();
      indexer.CreateIndex("20_newsgroups_subset", l, u, version, useGZIP);
   }

   /**
    * This method instantiates a new SimilarityEngine Object and then calls the
    * runSimilaritySearch method of that object.
    */
   private void runSimilaritySearch() {
      SimilarityEngine simEngine = new SimilarityEngine();

      simEngine.runSimilaritySearch("topics.txt", 10);
   }

   /**
    * Static Method that prints the usage message. This method is called, when
    * the user provides the --helper (resp. -h) option when calling the
    * application, or any invalid option or option argument is given
    */
   private static void printUsage() {
      System.out
            .println("Usage: java -jar task1.jar [--indexer] [--version=<small|medium|large>] [--upper=<upperThreshold>] [--lower=<lowerThreshold>] [--no-compress] [--help]");
   }

}
