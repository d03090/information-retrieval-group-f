#######################
#    1. Overview      #
#######################

The application uses GATE to parse through a corpus of documents to extract some information out of these documents.
The documents that are parsed are so-called CFP (Call for Papers), so they contain information about workshops and
conferences. 
The following information is extracted out of each document:
- Name of Workshop
- Acronym of Workshop
- Date of Workshop
- Location of Workshop
- Name of Conference
- Acronym of Conference
- Paper Submission Date of Workshop

To extract the information some JAPE rules have been written. They are located in the grammar-folder. The main.jape file
contains a list of the phases that are used, every phase is implemented in a seperate jape-file.
The application creates an output text-file that contains the extracted information for each document.


#############################
#    2. Run instructions    #
#############################

Short version:
--------------

Run the application with: java -jar task4.jar

This should be sufficient in most cases. You can however specify some parameters. Also it could be necessary to specify
the path to the GATE home directory. 
Please be advised that for the correct working of the GATE Developer a folder called "grammar" that contains the JAPE-rules
has to be located in the same directory as the jar-file.


Detailed version:
-----------------

The full synopsis of the application:

java [-Dgate.home=<path to GATE>] -jar task4.jar [--doc-count=<N>] [--corpus-dir=<corpusDirectory>] [--out-file=<output-file] [--help]

The default values for the parameters are the following:
doc-count = 5 (number of documents that are parsed in the pipeline at a time)
corpus-dir = train (relative path; all files in this directory will be parsed)
out-file = out.txt (relative path; this is the output-file that contains all gathered information)

The -Dgate.home option must be specified to set the value of the gate.home variable. This value must be set to the
installation directory of GATE. If the parameter is not set when calling the application, GATE will try to guess the
path to the GATE installation. This may work in some cases, but we didn't manage to make it work.

We successfully executed the application on a Windows 8 machine with the following command:

java -Dgate.home="C:\Program Files\GATE_Developer_7.1" -jar task4.jar


########################################
#    3. Important files and folders    #
########################################

An important folder is the grammar folder that contains all of the jape rules and that must be located in the same
directory as the jar-file. If this folder doesn't exist, the GATE Developer won't be able to load the JAPE files
for the final transducer.

Please also be advised that, if the application is called without any parameters so that the default values are used, 
there has to be a corpus-directory called train in the same directory as the jar-file is located. Also an output-file
called out.txt will be created in the same directory that the jar-file is in.