#######################
#    1. Overview      #
#######################

The application consists of two main parts. The indexer and the similarity detection engine.
Both are included in task2.jar, to seperately call them one must choose different calling options
(see section 2).

The indexer scans through the whole document collection and creates the arff files (respectively 
the compressed gzip files). This can take up to 10 minutes when no thresholds are specified.

The similarity search uses the index (the arff files) to find the top 10 most similar documents to 
some given input documents. 
NOTICE: Since Exercise 2 the similarity search will only use the large.arff.gz index file to search 
for similar documents. It is still possible to create other index files with any thresholds, but
these won't be used in search.


#############################
#    2. Run instructions    #
#############################

Short version:
--------------

Run the indexer with: java -jar task2.jar -i

Run the search with: java -jar task2.jar

Note: The topics.txt and the folder "20_newsgroups_subset" have to be in the same directory in which the jar file is located.


Details:
--------

Basic synopsis of the application:

java -jar task2.jar [--indexer] [--version=<small|medium|large>] [--upper=<upperThreshold>] [--lower=<lowerThreshold>] [--no-compress] [--help]

"java -jar task2.jar --help" will show this synopsis.

#### 2.1 indexer
To run the indexer, the option --indexer (resp. -i) must be provided. Also the version, the thresholds and the --no-compress can be chosen, 
but these options can also be omitted. The default values are: version=large, upper=Integer.MAX_VALUE, lower=0, no-compress=false
The version parameter leads to the name of the arff file, it should be either small, medium or large.
Upper and lower represent the thresholds for the collection frequency of terms. 
And if the no-compress option is provided, the resulting output file won't be compressed using gzip. Warning: The file size in this case can be up to 3 GB!
Attention: The location of the document-collection can not be specified by the user! The application always checks out the directory "20_newsgroups_subset".
So there must be a folder called "20_newsgroups_subset" in the same directory in which the jar file is located.

Examples for application calls for creating the three indexes:
java -jar task2.jar --indexer --version=small --lower=5 --upper=10
java -jar task2.jar --indexer --version=medium --lower=2 --upper=50
java -jar task2.jar --indexer --version=large

NOTICE: Since the similarity search in exercise 2 only uses the large.arff(.gz) file, it is sufficient to call the indexer like this:
java -jar task2.jar -i

This will generate the large.arff.gz file without any thresholds


#### 2.2 similarity search
To run the similarity search, it is sufficient to just omit the --indexer option, all other options will be ignored in that case (except for --help).
The similarity search will read the input-topics from a file called "topics.txt". The content of this file are the 20 topics given in the specification.
The output files will be created into the "out" folder. For every topic there will be one output file created.
The topics.txt must exist, contain valid documents and must be located in the same directory as the jar file for this process to function correctly!
NOTICE: Since exercise 2 the similarity search will only use the large.arff(.gz) file. So small and medium files will be ignored.
The similarity search will use the compressed arff file large.arff.gz by default, if no such file exists, the uncompressed file large.arff will be used.
If none of these files is found, the program will exit and the user is told to run the indexer first.

Example for calling the similarity search:
java -jar task2.jar


########################################
#    3. Important folders and files    #
########################################

All of the following files/directories must be located in the same directory as the jar file is in.

topics.txt - this file contains the 20 topics for the similarity search. must exist for similarity search to function.
20_newsgroups_subset - that's the root folder of the document collection. must exist for the indexer to function.
tmp - this folder contains the temporary block files of the indexing process. it is created when running the indexer. can be deleted after the index has been created.
out - contains the 20 output files of the similarity search. is created when the similarity search is called
large.arff(.gz) - the index file that is used by the similarity search. must exist for similarity search to function.


############################################################
#     4. Implementation Details and Tuning Parameters      #
############################################################

#### 4.1 Implementation Details

We have used the BM25 weighting scheme to implement the search engine.
We basically took the formula on page 35 in the lecture slides from the 15th of April and modified it a bit so that also the query (in our case the topic) is length-normalized.
This is done the following way: 
In the index-file we store for every (DocId, Term) pair a weight that is calculated by the following formula: Sqrt(log(N/df)) * (k + 1) * tf / (k*((1-b) + b*(Ld/Lave)) + tf)
So when we calculate the RSV for a specific document d in regards to a topic-document s (search) we iterate through all terms that are part of both documents and multiply
the weights for each term and then add them all up.
So resulting from the above formula the resulting formula, when multiplying the weights of a specific term t in the two documents, is as follows:
log(N/df) 
   * ((k + 1) * tf / (k*((1-b) + b*(Ld/Lave)) + tf)) 
   * ((k + 1) * tf / (k*((1-b) + b*(Ld/Lave)) + tf))

where the first factor is the idf of the term and the other two factors are specific to the corresponding document. So you see, this is almost the formula on page 35 in the lecture slides.
Another difference between our implementation and the formula in the slides is, that we don't (can't) make a difference between k1 and k3, so there's only k.
This is because of our data structure in the arff file.


#### 4.2 Tuning Parameters b and k

We tried out 9 different scenarios where the value of b was either 0.50, 0.75 or 1.00 and the value of k was either 1.2, 1.6 or 1.8
So considering all combinations of these values, there are altogether 9 different settings.
We submitted a results-folder for each scenario. The folders are called out_k=X_b=Y where X and Y stand for the different values of b respectively k.

We also made a little evaluation and compared the results for every parameter setting and every topic. The results of the evaluation are listed in the file "k-b.pdf".
We made a ranking of the parameter-settings for each topic and assigned high scores 
for very good settings and low scores for not so good settings. For example, the most accurate setting for a specific topic scored 9 points, the least accurate setting only 1 point.
We then added up the points from all topics for all settings and identified the settings with the most points.
To determine the high-scores for the settings, we looked at each document in the corresponding ranking-lists and rated their relevance subjectively. Starting from these subjective ratings and the orderings
of the ranking-lists we calculated the scores for each setting.

It showed that the two settings (k=1.2,b=0.75) and (k=1.6,b=0.75) had the highest scores and therefore delivered the most relevant documents according to our subjective evaluation.
Also it was obvious that all settings with b=0.5 delivered very bad subjective results. We believe that this is due to long documents in the ranking-lists that don't really contain
a lot of information regarding the search topic (verboseness). So it showed that b=0.5 is a too low value for the length normalization. but also b=1.0 didn't get as good results as b=0.75 did.
The k-parameter didn't seem to have such a massive impact on the results as the b-parameter apparently had.
Since (k=1.2,b=0.75) was one of the two highest scoring settings, we have hard-coded these values into the application. So when the indexer is called, they will be used.