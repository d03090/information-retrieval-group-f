package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * This application scans recursively through a directory named "corpus" that must 
 * be in the same directory as the application and writes the path of all contained 
 * files in that directory out into a txt-file called "song_list.txt". 
 * 
 * @author Leitner Thomas, Stangl Stefan
 */
public class Main {

   /**
    * The static main method of the application.
    * Here a new object of Main is instantiated and the
    * scanCorpus() method is called. The name of the song-corpus-directory
    * is given as "corpus" and the name of the output-file is 
    * "song_list.txt"
    */
   public static void main(String[] args) {
      Main m = new Main();
      try {
         m.scanCorpus("corpus", "song_list.txt");
      } catch (FileNotFoundException e) {
         e.printStackTrace();
      }
   }
   
   /**
    * This method scans through the corpus-directory and writes all file-paths
    * of the contained files into an output-file.
    * 
    * @param dirname the name of the corpus-directory
    * @param outFile the name of the output-file
    * @throws FileNotFoundException 
    */
   private void scanCorpus(String dirname, String outFile) throws FileNotFoundException {
      File rootDir = new File(dirname);
      
      if(rootDir.exists() && rootDir.isDirectory()) {
         PrintWriter pw = new PrintWriter(outFile);
         scanCorpus(rootDir, pw);
         pw.close();
      } else {
         System.out.println("ERROR: root-directory called \"corpus\" must exist!");
      }
   }
   
   /**
    * This method scans through all files of the the directory "folder" 
    * and writes their relative paths out into an output-file pointed
    * to by "pw". If a sub-folder is found, the method is called recursively.
    * 
    * @param folder the folder that is used for the search
    * @param pw the PrintWriter object for the output-file
    */
   private void scanCorpus(File folder, PrintWriter pw) {
      for(File file : folder.listFiles()) {
         if(file.isDirectory()) {
            scanCorpus(file, pw);
         } else {
            pw.println(file.getPath());
         }
      }
   }

}
